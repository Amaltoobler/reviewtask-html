const express=require("express")
const Collection=require("./src/mongo")

const app=express()
const path=require("path")
const jwt=require("jsonwebtoken")
const cookieParser=require("cookie-parser")
const bcryptjs=require("bcryptjs")
const multer = require("multer")

app.use(express.json())
app.use(cookieParser())
app.use(express.urlencoded({extended:false}))
app.use(express.static('images'));



const tempelatePath=path.join(__dirname,"./src/tempelates")
const publicPath=path.join(__dirname,"./src/public")

app.set('view engine','ejs')
app.set("views",tempelatePath)
app.use(express.static(publicPath))



async function hashPass(password){

    const res=await bcryptjs.hash(password,10)
    return res

}
async function compare(userPass,hashPass){

    const res=await bcryptjs.compare(userPass,hashPass)
    return res

}



app.get("/",(req,res)=>{

    if(req.cookies.jwt){
        const verify=jwt.verify(req.cookies.jwt,"helloandwelcometotechywebdevtutorialonauthhelloandwelcometotechywebdevtutorialonauth")
    res.render("home",{name:verify.name})
    }

    else{
        res.render("login")
    }

})
app.get("/signup",(req,res)=>{
    res.render("signup")
})



const storage=multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'src/images')
    },
    filename:(req,file,cb)=>{
        ff=file
        cb(null,ff.originalname)
    }
})

const upload=multer({storage:storage})

app.post("/signup",  upload.single("image"),async(req,res)=>{
    try{
        let img;
        const check=await Collection.findOne({name:req.body.name})
        if(check){
            res.send("user already exist")
        }
        else{
            const token=jwt.sign({name:req.body.name},"helloandwelcometotechywebdevtutorialonauthhelloandwelcometotechywebdevtutorialonauth")

            res.cookie("jwt",token,{
                maxAge:600,
                httpOnly:true
            })

            if((ff.mimetype).split('/').pop()=="png" || (ff.mimetype).split('/').pop()=="jpg" || (ff.mimetype).split('/').pop()=="jpeg"){

                
             img = ff.originalname
            }
            else{
                res.send("inavlid file")
            }
            const data={
                name:req.body.name,
                address:req.body.address,
                age:req.body.age,
                phonenumber:req.body.phonenumber,
                password:await hashPass(req.body.password),
                path:img,
                token:token,
                
            }
            await Collection.create(data)
            res.render("home",{data:data})
        }

    }
    catch{

        res.send("wrong details")

    }
})


app.post("/login",async(req,res)=>{
    try{
        const check=await Collection.findOne({name:req.body.name})
        const passCheck=await compare(req.body.password,check.password)

        if(check && passCheck){

            res.cookie("jwt",check.token,{
                maxAge:600,
                httpOnly:true
            })

            res.render("home",{data:req.body.name})
        }
        
        else{
            
            res.send("wrong details")

        }

    }
    catch{

        res.send("wrong details")

    }
})

// let arr=[]


// app.get("/",async(req,res)=>{
//     const data=await Collection.find()
//     arr=data
//     res.render('home',{arr:data})
// })
app.listen(8000,()=>{
    console.log("port connected")
})
