const mongoose=require("mongoose")

mongoose.connect("mongodb://localhost:27017/AuthTut")
.then(()=>{
    console.log("mongo connected")
})
.catch(()=>{
    console.log("error")
})

const Schema=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    address: {
        type: String,
        require: true
    },
    age: {
        type: String,
        require: true
    },
    phonenumber: {
        type: String,
        require: true
    },

    password:{
        type:String,
        required:true
    },
    
    path:{
        type:String,
        required:true
    },

    token:{
        type:String,
        required:true
    }
})

const Collection=new mongoose.model("AuthCollection",Schema)

module.exports=Collection
